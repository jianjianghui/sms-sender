package com.leetao.sms.demo;

import com.leetao.sms.core.domain.Recipients;
import com.leetao.sms.core.domain.SmsParams;
import com.leetao.sms.core.domain.SmsResult;
import com.leetao.sms.core.domain.TemplateSms;
import com.leetao.sms.core.support.sender.SmsSender;
import com.leetao.sms.springboot.config.SmsProperties;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

/**
 * test
 *
 * @author 菅江晖
 * @date 2021/6/7 - 14:34
 */
@SpringBootTest
public class test {

    @Autowired
    private SmsProperties smsProperties;
    @Autowired
    private SmsSender smsSender;

    @Test
    public void tencentTest() {
        //  application.yml jjh.sms.active: tencent
        SmsResult smsResult = smsSender.sendSmsByTemplate(TemplateSms.instance()
                .recipients(Recipients.create().addTo("+8613200000000"))
                .templateId("000000")
                .smsParams(SmsParams.create().add("value").add("value")));
    }

    @Test
    public void aliyunTest() {
        //  application.yml jjh.sms.active: aliyun
        SmsResult smsResult = smsSender.sendSmsByTemplate(TemplateSms.instance()
                .recipients(Recipients.create().addTo("13000000000"))
                .templateId("SMS_000000000")
                .smsParams(SmsParams.create().add("key", "value")));
    }

    @Test
    public void baiduTest() {
        //  application.yml jjh.sms.active: baidu
        SmsResult smsResult = smsSender.sendSmsByTemplate(TemplateSms.instance()
                .recipients(Recipients.create().addTo("13000000000"))
                .templateId("sms-tmpl-0000000000")
                .smsParams(SmsParams.create().add("key", "value").add("key", "value")));
    }

    @Test
    public void test() {
        System.out.println(UUID.randomUUID());

    }

}
