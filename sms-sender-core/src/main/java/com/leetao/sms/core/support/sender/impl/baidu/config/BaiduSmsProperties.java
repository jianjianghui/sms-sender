package com.leetao.sms.core.support.sender.impl.baidu.config;

/**
 * 百度短信配置
 *
 * @author 菅江晖
 * @date 2021/6/9 - 9:28
 */
public class BaiduSmsProperties {
    private String accessKeyId;
    private String secretAccessKey;
    /**
     * 签名id
     */
    private String signatureId;

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getSecretAccessKey() {
        return secretAccessKey;
    }

    public void setSecretAccessKey(String secretAccessKey) {
        this.secretAccessKey = secretAccessKey;
    }

    public String getSignatureId() {
        return signatureId;
    }

    public void setSignatureId(String signatureId) {
        this.signatureId = signatureId;
    }
}
