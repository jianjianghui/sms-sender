package com.leetao.sms.core.domain;

/**
 * 模板短信
 *
 * @author 菅江晖
 * @date 2021/6/9 - 10:00
 */
public class TemplateSms {

    /**
     * 模板id
     */
    private String templateId;

    /**
     * 接收人
     */
    private Recipients recipients;

    /**
     * 短信参数
     */
    private SmsParams smsParams;

    private TemplateSms(String templateId, Recipients recipients, SmsParams smsParams) {
        this.templateId = templateId;
        this.recipients = recipients;
        this.smsParams = smsParams;
    }

    public TemplateSms() {
    }

    public String getTemplateId() {
        return templateId;
    }

    public Recipients getRecipients() {
        return recipients;
    }

    public SmsParams getSmsParams() {
        return smsParams;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public void setRecipients(Recipients recipients) {
        this.recipients = recipients;
    }

    public void setSmsParams(SmsParams smsParams) {
        this.smsParams = smsParams;
    }

    /**
     * 模板id
     *
     * @param templateId templateId
     * @return this
     */
    public TemplateSms templateId(String templateId) {
        this.templateId = templateId;
        return this;
    }

    /**
     * 接收人
     *
     * @param recipients recipients
     * @return this
     */
    public TemplateSms recipients(Recipients recipients) {
        this.recipients = recipients;
        return this;
    }

    /**
     * 短信参数
     *
     * @param smsParams smsParams
     * @return this
     */
    public TemplateSms smsParams(SmsParams smsParams) {
        this.smsParams = smsParams;
        return this;
    }

    public static TemplateSms instance() {
        return new TemplateSms();
    }
}
