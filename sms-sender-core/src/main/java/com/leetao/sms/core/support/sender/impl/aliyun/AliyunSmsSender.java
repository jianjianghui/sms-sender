package com.leetao.sms.core.support.sender.impl.aliyun;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;
import com.leetao.sms.core.domain.SmsParams;
import com.leetao.sms.core.domain.SmsResult;
import com.leetao.sms.core.domain.TemplateSms;
import com.leetao.sms.core.exception.SmsException;
import com.leetao.sms.core.support.sender.impl.aliyun.config.AliyunSmsProperties;
import com.leetao.sms.core.util.GsonUtils;
import com.leetao.sms.core.constant.SmsExceptionEnums;
import com.leetao.sms.core.domain.Recipients;
import com.leetao.sms.core.support.sender.SmsSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

/**
 * 阿里云短信发送器
 *
 * @author 菅江晖
 * @date 2021/6/7 - 11:34
 */
public class AliyunSmsSender implements SmsSender {
    private static final Logger LOG = LoggerFactory.getLogger(AliyunSmsSender.class);
    private static final String DELIMITER = ",";
    private static final String SEND_SUCCESSFUL_TAG = "OK";
    private static final String ENDPOINT = "dysmsapi.aliyuncs.com";

    private final Client aliyunClient;
    private final AliyunSmsProperties properties;

    public AliyunSmsSender(AliyunSmsProperties properties) {
        this.properties = properties;
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(properties.getAccessKeyId())
                // 您的AccessKey Secret
                .setAccessKeySecret(properties.getAccessKeySecret());
        // 访问的域名
        config.setEndpoint(ENDPOINT);
        this.aliyunClient = createSender(config);
    }

    private Client createSender(Config config) {
        try {
            return new Client(config);
        } catch (Exception e) {
            return null;
        }

    }


    @Override
    public Function<SmsParams, String> convertAvailableSmsParameters() {
        return GsonUtils::toJson;
    }

    @Override
    public SmsResult sendSmsByTemplate(TemplateSms templateSms) throws SmsException {
        String templateId = templateSms.getTemplateId();
        SmsParams smsParams = templateSms.getSmsParams();
        Recipients recipients = templateSms.getRecipients();
        LOG.trace("aliyun sms request:{}", GsonUtils.toJson(templateSms));
        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setPhoneNumbers(String.join(DELIMITER, recipients))
                .setSignName(properties.getSignName())
                .setTemplateCode(templateId)
                .setTemplateParam(convertAvailableSmsParameters().apply(smsParams));

        try {
            SendSmsResponse sendSmsResponse = aliyunClient.sendSms(sendSmsRequest);
            LOG.trace("aliyun sms response:{}", GsonUtils.toJson(sendSmsResponse));
            return SmsResult.create(SEND_SUCCESSFUL_TAG.equals(sendSmsResponse.getBody().getCode()), sendSmsResponse).setMessage(sendSmsResponse.getBody().getMessage());

        } catch (Exception e) {
            throw SmsException.because(e.getMessage());
        }
    }

    @Override
    public SmsResult sendSms(Recipients recipients, String context) throws SmsException {
        throw SmsException.because(SmsExceptionEnums.NOT_SUPPORT);
    }
}
