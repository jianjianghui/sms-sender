package com.leetao.sms.core.support.sender.impl.tencent.support;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20210111.SmsClient;

/**
 * 腾讯短信客户端构建器
 *
 * @author 菅江晖
 * @date 2021/6/7 - 16:45
 */
public class TencentSmsClientBuilder {
    private static final String ENDPOINT = "sms.tencentcloudapi.com";

    private final Credential cred;

    private String region;


    public TencentSmsClientBuilder(String secretId, String secretKey) {
        this.cred = new Credential(secretId, secretKey);
    }

    public TencentSmsClientBuilder region(String region) {
        this.region = region;
        return this;
    }

    public SmsClient build() {
        return new SmsClient(cred, region, ClientProperties.endpoint(ENDPOINT));
    }

    public static TencentSmsClientBuilder create(String secretId, String secretKey) {
        return new TencentSmsClientBuilder(secretId, secretKey);
    }


    /**
     * 封装一层
     */
    public static class ClientProperties extends ClientProfile {
        private static final HttpProfile HTTP_PROFILE = new HttpProfile();
        private static final ClientProperties INSTANCE = new ClientProperties();


        public static ClientProperties endpoint(String endpoint) {
            HTTP_PROFILE.setEndpoint(endpoint);
            INSTANCE.setHttpProfile(HTTP_PROFILE);
            return INSTANCE;
        }

    }
}
