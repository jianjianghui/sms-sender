package com.leetao.sms.core.support.sender.impl.tencent.config;

/**
 * 腾讯短信
 *
 * @author 菅江晖
 * @date 2021/6/7 - 11:41
 */
public class TencentSmsProperties {

    /**
     * SecretId
     */
    private String secretId;

    /**
     * SecretKey
     */
    private String secretKey;

    /**
     * 地域参数，建议您阅读文档了解地域以及计费情况
     */
    private String region = "ap-guangzhou";

    /**
     * 短信 SdkAppId
     */
    private String smsSdkAppId;

    /**
     * 短信签名内容
     */
    private String signName;

    public String getSecretId() {
        return secretId;
    }

    public void setSecretId(String secretId) {
        this.secretId = secretId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSmsSdkAppId() {
        return smsSdkAppId;
    }

    public void setSmsSdkAppId(String smsSdkAppId) {
        this.smsSdkAppId = smsSdkAppId;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }
}

