package com.leetao.sms.core.domain;

/**
 * 短信结果
 *
 * @author 菅江晖
 * @date 2021/6/7 - 10:57
 */
public class SmsResult {

    /**
     * 是否成功
     */
    boolean successful;

    /**
     * 消息
     */
    String message;

    /**
     * 结果
     */
    Object result;

    public SmsResult(boolean successful, Object result) {
        this.successful = successful;
        this.result = result;
    }

    public SmsResult() {
    }

    public static SmsResult create(boolean successful, Object result) {
        return new SmsResult(successful, result);
    }

    public String getMessage() {
        return message;
    }

    public SmsResult setMessage(String message) {
        this.message = message;
        return this;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SmsResult{");
        sb.append("successful=").append(successful);
        sb.append(", message='").append(message).append('\'');
        sb.append(", result=").append(result);
        sb.append('}');
        return sb.toString();
    }
}
