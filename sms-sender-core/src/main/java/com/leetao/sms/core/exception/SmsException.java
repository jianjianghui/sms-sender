package com.leetao.sms.core.exception;

import com.leetao.sms.core.constant.SmsExceptionEnums;

/**
 * 短信异常
 *
 * @author 菅江晖
 * @date 2021/6/7 - 11:01
 */
public class SmsException extends RuntimeException {
    final int code;
    final String msg;

    public SmsException(SmsExceptionEnums enums) {
        super(enums.getMsg());
        this.code = enums.getCode();
        this.msg = enums.getMsg();
    }

    public SmsException(int code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }


    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public static SmsException because(SmsExceptionEnums enums) {
        return new SmsException(enums);
    }

    public static SmsException because(String message) {
        return new SmsException(-1, message);
    }

}
