package com.leetao.sms.core.support.sender.impl.baidu;

import com.baidubce.auth.DefaultBceCredentials;
import com.baidubce.services.sms.SmsClient;
import com.baidubce.services.sms.SmsClientConfiguration;
import com.baidubce.services.sms.model.SendMessageV3Request;
import com.baidubce.services.sms.model.SendMessageV3Response;
import com.leetao.sms.core.constant.SmsExceptionEnums;
import com.leetao.sms.core.domain.Recipients;
import com.leetao.sms.core.domain.SmsParams;
import com.leetao.sms.core.domain.SmsResult;
import com.leetao.sms.core.domain.TemplateSms;
import com.leetao.sms.core.exception.SmsException;
import com.leetao.sms.core.support.sender.SmsSender;
import com.leetao.sms.core.support.sender.impl.baidu.config.BaiduSmsProperties;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

/**
 * 百度短信发送器
 *
 * @author 菅江晖
 * @date 2021/6/9 - 9:17
 */
public class BaiDuSmsSender implements SmsSender {
    private static final String ENDPOINT = "http://smsv3.bj.baidubce.com";
    private static final String DELIMITER = ",";

    private final SmsClient baiduCilent;
    private final BaiduSmsProperties properties;


    public BaiDuSmsSender(BaiduSmsProperties properties) {
        SmsClientConfiguration config = new SmsClientConfiguration();
        config.setCredentials(new DefaultBceCredentials(properties.getAccessKeyId(), properties.getSecretAccessKey()));
        config.setEndpoint(ENDPOINT);
        this.baiduCilent = new SmsClient(config);
        this.properties = properties;
    }

    @Override
    public Function<SmsParams, Map<String, String>> convertAvailableSmsParameters() {
        return smsParams -> {
            Map<String, String> newMap = new LinkedHashMap<>();
            smsParams.forEach((s, o) -> newMap.put(s, o.toString()));
            return newMap;
        };
    }

    @Override
    public SmsResult sendSmsByTemplate(TemplateSms templateSms) throws SmsException {
        String templateId = templateSms.getTemplateId();
        SmsParams smsParams = templateSms.getSmsParams();
        Recipients recipients = templateSms.getRecipients();

        SendMessageV3Request request = new SendMessageV3Request();
        request.setMobile(String.join(DELIMITER, recipients));
        request.setSignatureId(properties.getSignatureId());
        request.setTemplate(templateId);
        request.setContentVar(this.convertAvailableSmsParameters().apply(smsParams));
        SendMessageV3Response response = baiduCilent.sendMessage(request);
        // 解析请求响应 response.isSuccess()为true 表示成功
        if (response != null && response.isSuccess()) {
            return SmsResult.create(true, response);
        }

        return SmsResult.create(false, response).setMessage(Optional.ofNullable(response).isPresent() ? response.getMessage() : null);
    }

    @Override
    public SmsResult sendSms(Recipients recipients, String context) throws SmsException {
        throw SmsException.because(SmsExceptionEnums.NOT_SUPPORT);
    }

}
