package com.leetao.sms.core.constant;

/**
 * 短信异常枚举
 * @author 菅江晖
 * @date 2021/6/7 - 11:12
 */
public enum SmsExceptionEnums {
    /**
     * 不支持
     */
    NOT_SUPPORT(50001,"不支持此操作");


    int code;
    String msg;

    SmsExceptionEnums(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }


    public String getMsg() {
        return msg;
    }

}
