package com.leetao.sms.core.constant;

/**
 * 短信常量
 *
 * @author 菅江晖
 * @date 2021/6/7 - 10:22
 */
public class SmsConstants {
    /**
     * 阿里云
     */
    public static final String ALIYUN = "aliyun";

    /**
     * 腾讯
     */
    public static final String TENCENT = "tencent";


}
