package com.leetao.sms.core.domain;

import java.util.LinkedList;

/**
 * 接收者
 *
 * @author 菅江晖
 * @date 2021/6/7 - 11:16
 */
public class Recipients extends LinkedList<String> {
    public Recipients addTo(String key) {
        this.add(key);
        return this;
    }

    public static Recipients create() {
        return new Recipients();
    }

}
