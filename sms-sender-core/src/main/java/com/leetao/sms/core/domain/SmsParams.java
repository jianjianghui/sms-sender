package com.leetao.sms.core.domain;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.UUID;

/**
 * 短信参数
 *
 * @author 菅江晖
 * @date 2021/6/7 - 10:50
 */
public class SmsParams extends LinkedHashMap<String, Object> {

    public SmsParams add(String key, Object value) {
        this.put(key, value);
        return this;
    }

    /**
     * 添加value(key 自动生成)
     *
     * @param value value
     * @return this
     */
    public SmsParams add(Object value) {
        this.put(UUID.randomUUID().toString(), value);
        return this;
    }

    public static SmsParams create() {
        return new SmsParams();
    }

}
