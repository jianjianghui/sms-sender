package com.leetao.sms.core.util;

import com.google.gson.Gson;

/**
 * @author 菅江晖
 * @date 2021/6/7 - 13:31
 */
public class GsonUtils {
    private final static Gson GSON = new Gson();


    /**
     * 对象转json串
     */
    public static String toJson(Object o) {
        return GSON.toJson(o);
    }

    /**
     * 从json串封装对象
     */
    public static <T> T fromJson(String s, Class<T> cls) {
        return GSON.fromJson(s, cls);
    }

}
