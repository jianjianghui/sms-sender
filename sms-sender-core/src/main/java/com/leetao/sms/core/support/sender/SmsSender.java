package com.leetao.sms.core.support.sender;

import com.leetao.sms.core.domain.SmsParams;
import com.leetao.sms.core.domain.SmsResult;
import com.leetao.sms.core.domain.TemplateSms;
import com.leetao.sms.core.exception.SmsException;
import com.leetao.sms.core.domain.Recipients;


import java.util.function.Function;

/**
 * 短信发送器
 *
 * @author 菅江晖
 * @date 2021/6/7 - 10:48
 */
public interface SmsSender {


    /**
     * 将SmsParams 转换为 指定smsSender可用的短信参数
     *
     * @return 可用的短信参数
     */
    <T> Function<SmsParams, T> convertAvailableSmsParameters();

    /**
     * 通过模板id发送短信
     *
     * @param templateSms 模板短信
     * @return smsResult
     * @throws SmsException 短信异常
     */
    SmsResult sendSmsByTemplate(TemplateSms templateSms) throws SmsException;


    /**
     * @param recipients 接收者(list)
     * @param context    内容
     * @return sendSms
     * @throws SmsException 短信异常
     */
    SmsResult sendSms(Recipients recipients, String context) throws SmsException;

}
