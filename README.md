# smsSender
短信发送统一规范
已集成平台：阿里云短信，腾讯云短信。

内附SpringBoot 启动器 开箱即用（默认为所有短信发送器,如果你需要只需要发送阿里云的短信，你也可以通过core模块单独创建 例如:aliyun-sms-springboot-starter)）

也可通过core模块使用(拓展其他框架,Jfinal....)

详见demo 