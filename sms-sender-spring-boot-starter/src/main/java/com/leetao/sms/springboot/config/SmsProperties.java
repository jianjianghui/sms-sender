package com.leetao.sms.springboot.config;

import com.leetao.sms.core.support.sender.impl.aliyun.config.AliyunSmsProperties;
import com.leetao.sms.core.support.sender.impl.baidu.config.BaiduSmsProperties;
import com.leetao.sms.core.support.sender.impl.tencent.config.TencentSmsProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * 短信发送配置属性
 *
 * @author 菅江晖
 * @date 2021/6/7 - 10:03
 */
@ConfigurationProperties(prefix = "jjh.sms")
public class SmsProperties {
    /**
     * 是否启用
     */
    boolean enable = false;

    /**
     * 当前启用
     */
    String active;

    /**
     * 腾讯
     */
    Tencent tencent;

    /**
     * 阿里云
     */
    Aliyun aliyun;

    /**
     * 百度
     */
    Baidu baidu;


    public boolean isEnable() {
        return enable;
    }

    public boolean setEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }


    public Tencent getTencent() {
        return tencent;
    }

    public void setTencent(Tencent tencent) {
        this.tencent = tencent;
    }

    public Aliyun getAliyun() {
        return aliyun;
    }

    public void setAliyun(Aliyun aliyun) {
        this.aliyun = aliyun;
    }

    public Baidu getBaidu() {
        return baidu;
    }

    public void setBaidu(Baidu baidu) {
        this.baidu = baidu;
    }

    public static class Aliyun extends AliyunSmsProperties {
    }

    public static class Tencent extends TencentSmsProperties {
    }

    public static class Baidu extends BaiduSmsProperties {
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SmsProperties{");
        sb.append("enable=").append(enable);
        sb.append(", active='").append(active).append('\'');
        sb.append(", tencent=").append(tencent);
        sb.append(", aliyun=").append(aliyun);
        sb.append('}');
        return sb.toString();
    }
}
