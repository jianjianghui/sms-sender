package com.leetao.sms.springboot;

import com.leetao.sms.core.constant.SmsConstants;
import com.leetao.sms.core.support.sender.SmsSender;
import com.leetao.sms.core.support.sender.impl.aliyun.AliyunSmsSender;
import com.leetao.sms.core.support.sender.impl.baidu.BaiDuSmsSender;
import com.leetao.sms.core.support.sender.impl.tencent.TencentSmsSender;
import com.leetao.sms.springboot.config.SmsProperties;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;

/**
 * 短信自动配置
 *
 * @author 菅江晖
 * @date 2021/6/7 - 13:58
 */
@SpringBootConfiguration(proxyBeanMethods = false)
@EnableConfigurationProperties(SmsProperties.class)
public class SmsAutoConfiguration {

    private final SmsProperties properties;

    public SmsAutoConfiguration(SmsProperties properties) {
        this.properties = properties;
    }

    @Bean
    @ConditionalOnMissingBean(SmsSender.class)
    @ConditionalOnExpression("'${jjh.sms.active}'.equals('tencent') && ${jjh.sms.enable:true}")
    public SmsSender tencentSmsSender() {
        return new TencentSmsSender(properties.getTencent());
    }

    @Bean
    @ConditionalOnMissingBean(SmsSender.class)
    @ConditionalOnExpression("'${jjh.sms.active}'.equals('aliyun') && ${jjh.sms.enable:true}")
    public SmsSender aliyunSmsSender() {
        return new AliyunSmsSender(properties.getAliyun());
    }

    @Bean
    @ConditionalOnMissingBean(SmsSender.class)
    @ConditionalOnExpression("'${jjh.sms.active}'.equals('baidu') && ${jjh.sms.enable:true}")
    public SmsSender baiduSmsSender() {
        return new BaiDuSmsSender(properties.getBaidu());
    }


}
